﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace switchWithUserInput
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("[1] - Convert distance from Kilometers to Miles");
            Console.WriteLine("[2] - Convert distance from Miles to Kilometers");

            Console.WriteLine("\nEnter an option:");
            var option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    const double kmtoMiles = 0.621371;
                    double km = 0.0;
                    double result1 = 0.0;

                    Console.WriteLine("\nEnter a distance in Kilometers:");
                    km = double.Parse(Console.ReadLine());

                    result1 = km * kmtoMiles;

                    Console.WriteLine($"\n{km} Kilometers = {result1} Miles");

                    break;

                case "2":
                    const double mtoKM = 1.609344;
                    double m = 0.0;
                    double result2 = 0.0;

                    Console.WriteLine("\nEnter a distance in Miles:");
                    m = double.Parse(Console.ReadLine());

                    result2 = m * mtoKM;

                    Console.WriteLine($"\n{m} Miles = {result2} Kilometers");

                    break;

                default:
                    Console.WriteLine($"\nErr: Invalid option!");

                    break;
            } // End case Switch
        }
    }
}
